from google import google
import urllib2
import re
from django.template import Template, Context
from django.conf import settings
settings.configure()

address_regex = r'Address.+(\b[\d]{4,5}.+OR \d{5})'
price_regex = r'Price.*?\$([\d,]?\d{3},\d{3})'
rmls_img_regex = r'<img title=.Image of Multiple Listing .*? src=.(\S*).'


class House():

    def __init__(self):
        self.image = ''
        self.rmls_num = ''
        self.price = ''
        self.address = ''
        self.photos = []
        self.links = []


class RmlsFinder():

    def __init__(self):
        self.data = []
        self.house = House()

    def search_addr(self, addr):
        g = google.search("{} site:www.redfin.com".format(addr), 1)
        red_site = g[0].google_link
        req = urllib2.Request(red_site, headers={'User-Agent': 'Chrome'})
        raw = urllib2.urlopen(req).read()
        rmls_num = re.search("RMLS #(\d{8})", raw)
        if rmls_num is None:
            print "error finding address {}".format(addr)
        else:
            self.search_rmls(rmls_num.group(1))

    def search_rmls(self, rmls_num):
        rmls_site = "http://www.rmls.com/report/{}".format(rmls_num)
        try:
            req = urllib2.Request(rmls_site, headers={'User-Agent': 'Chrome'})
            raw = urllib2.urlopen(req).read()
        except urllib2.URLError:
            print "Error connecting to internet"
        addr = re.search(address_regex, raw)
        if not addr:
            print "error finding {}".format(rmls_num)
            return
        addr = addr.group(1).replace("<br \>", " ")
        price = re.search(price_regex, raw)
        price = price.group(1)
        self.house.price = price
        img = re.search(rmls_img_regex, raw)
        img = img.group(1).rstrip("'")

        self.house.image = "http://www.rmls.com{}".format(img)

        g = google.search("{} site:www.redfin.com".format(addr), 1)
        red_site = g[0].google_link if g else "http://www.redfin.com"

        self.house.rmls_num = rmls_num
        self.house.address = addr
        self.house.links.append({"name": "RMLS", "site": rmls_site})
        self.house.links.append({"name": "RedFin", "site": red_site})


addrs = raw_input("Please enter rmls number or address\nCSV are allowed: ").strip()

houses = []

for addr in addrs.split(","):
    addr = addr.strip()
    rmls = RmlsFinder()
    if re.search("(\d{8})", addr):
        rmls.search_rmls(addr)
    else:
        rmls.search_addr(addr)
    houses.append(rmls.house)

t = Template(open('template.html').read())
c = Context({"title": "House Details",
             "houses": houses})
f = open('details.html', 'w')
f.write(t.render(c))
f.close()

#for line in rmls.data:
#    print line